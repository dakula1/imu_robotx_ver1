# README #

This package is used for reading data from a xsens MTi-G, and publish the data on the imu_message topic so that various modules can access this data. This package was developed by Dhanraj Akula on the Villanova/FAU team for the 2016 RobotX Competition.

This code is provided as a ROS (Indigo) package which is dependent on the following ROS packages:

    roscpp
    rospy
    std_msgs and custom message (robotx_imu_msg)

This package is compiled using catkin. To compile it, place this repository in the directory (your catkin workspace)/src, and then run catkin_make from your top-level catkin directory:


#Using this Project#

Before running the ros package make sure that the name of the USB port is declared correctly in the xsens_read.py file 

port = '/dev/ttyUSB0' this should be set to correct port in order for the computer to read the sensor data.

After compiling this package, start a roscore session.

Run rosrun imu_robotx_ver1 robotx_imu_publisher_node.py.This will read the data from the IMU sensor and publish the sensor data on the 'imu_message' topic and also store the data in an xsls file.

 NOTE:For testing purposes the data which is read from the sensor are also       stored in the xsls file.In order to do so i have imported the xlsxwriter library in python.So make sure you have the this library.