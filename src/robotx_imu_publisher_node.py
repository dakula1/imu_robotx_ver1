#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
import xsens_read
import xlsxwriter #Used to store the data into a xls file
from imu_robotx_ver1.msg import robotx_imu_msg
import time


workbook = xlsxwriter.Workbook('Experiment_test_data.xlsx')
worksheet = workbook.add_worksheet()

def read_imu() :
    #defining a publisher pub with topic name imu_message and message time robotx_imu_msg
    pub = rospy.Publisher('imu_message', robotx_imu_msg, queue_size=10)
    rospy.init_node('read_data', anonymous=True)
    #defining the rate at which the data is received 
    rate = rospy.Rate(100) # 100hz
    # iterates through the loop infinitely and published the IMU sensor data 
    row=0
    col=0
    while not rospy.is_shutdown():
        #Calling the function xsens_imu_data() from the file xsens_read.py which returns the list which contains
        #[temp,accx,accy,accz,gyrox,gyroy,gyroz,roll,pitch,yaw,latitude,longitude,altitude,velx,vely,velz] in the specified order
        #storing the data in buffer
        buffer=[]
    	buffer=xsens_read.xsens_imu_data()
        #creating an object of the message type robotx_imu_msg and assigning the values to each feild of the message type 
    	imu_message=robotx_imu_msg()
    	imu_message.temp=(buffer[0])
    	imu_message.accx=(buffer[1])
    	imu_message.accy=(buffer[2])
	imu_message.accz=(buffer[3])
    	imu_message.gyrox=(buffer[4])
    	imu_message.gyroy=(buffer[5])
	imu_message.gyroz=(buffer[6])	
    	imu_message.roll=(buffer[7])
    	imu_message.pitch=(buffer[8])
	imu_message.yaw=(buffer[9])
    	imu_message.latitude=(buffer[10])
    	imu_message.longitude=(buffer[11])
	imu_message.altitude=float(buffer[12])
    	imu_message.velx=(buffer[13])
    	imu_message.vely=(buffer[14])
    	imu_message.velz=(buffer[15])
    	imu_message.utc_time=(buffer[16])
    	localtime = time.asctime( time.localtime(time.time()) )
    	imu_message.sys_time=localtime
    	#to print the data on the console 
    	#rospy.loginfo(imu_message)
    	#storing the data in a xls file 
    	buffer.append(localtime)
    	for val in buffer:
    	    worksheet.write(row+1,col,val)
	    col=col+1
	row=row+1 
        col=0    	
    	#publishing the data imu_message
        pub.publish(imu_message)
        rate.sleep()

if __name__ == '__main__':
    try:
        read_imu()
        workbook.close()
    except rospy.ROSInterruptException:
        workbook.close()
        pass
