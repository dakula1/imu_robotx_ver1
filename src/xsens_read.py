# This script is used to read the data from the xsens IMU
# using configuration settings:
#
# North, East, Down should be enabled
# 16.32 output type
#
# Data passed is as follows:
# [Temp]
# [Calibrated Data] - accelerations and gyroscopic (no magnetic)
# [Orientation Data] - euler
# [Position Data]
# [Velocity Data]
# [UTC Time]
#
# Timestamp uses 12 bytes
# All other data (16 total readings) use 6 bytes each
# Total data bytes: 108 bytes, which is 6C in hex

import serial  # Reads the sensor
import struct  # Parses bytes into integers
import math  # Used for bit shift
import os  # Clear screen command
import time  # Timer for timeout
import xlsxwriter #Used to store the data into a xls file 
import time


class MTException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return "MT error:  " + self.message


# Function for reading 16.32 formatted data
def convert16_32(data_bytes):
    o = {}
    o['int'], = struct.unpack('!h', data_bytes[4:])
    o['dec'], = struct.unpack('!I', data_bytes[:4])
    data = o['int'] + (o['dec'] / math.pow(2, 32))
    return data


# Pare the UTC timestamp
def parse_timestamp(content):
    o = {}
    o['ns'], o['Year'], o['Month'], o['Day'], o['Hour'],\
        o['Minute'], o['Second'], o['Flags'] =\
        struct.unpack('!LHBBBBBB', content)
    return o

# Low-level MTData receiving function.
# Take advantage of known message length.
def read_data_msg(length, buf):
    """Low-level MTData receiving function.
    Take advantage of known message length."""
    totlength = 5 + length  # Including the header, message info, and checksum
    # Start timer
    start = time.time()
    check_timeout=time.time()-start
    while (check_timeout) < timeout:
        buf.extend(ser.read(totlength-len(buf)))
        preamble_ind = buf.find(header)
        if preamble_ind == -1:  # not found
            del buf[:-3]
            continue
        elif preamble_ind:  # found but not at start
            del buf[:preamble_ind]  # discard leading bits
            # complete message for checksum
            while len(buf) < totlength:
                buf.extend(ser.read(totlength-len(buf)))
        if 0xFF & sum(buf[1:]):
            # Invalid checksum
            del buf[:buf.find(header)-2]
            continue
        data = str(buf[-length-1:-1])
        del buf[:]
        return data
    else:
        print "The current time is :"
        print time.time()
        input(check_timeout)
        workbook.close()
        raise MTException("could not find MTData message.")
        


length = 108  # Number of bytes of data message


# Serial values
port = '/dev/ttyUSB0'

baudrate = 921600
timeout = 0.01
header = '\xFA\xFF\x32'+chr(length)

ser = 	(port, baudrate, timeout=timeout, writeTimeout=timeout)
ser.flushInput()
ser.flushOutput()
def xsens_imu_data ():
    # Initaialize empty byte array as buffer
    buf = bytearray()
    # Read in the new data
    data = read_data_msg(length, buf)
    # Clear old data off screen
    os.system('clear')
    data_inds = range(0, length - 11, 6)
    # Parse Temperature data and display
    t = {}
    t['Temp'] = convert16_32(data[data_inds[0]: data_inds[0] + 6])
    # Parse Calibrated data and display
    c = {}
    c['AccX'] = convert16_32(data[data_inds[1]: data_inds[1] + 6])
    c['AccY'] = convert16_32(data[data_inds[2]: data_inds[2] + 6])
    c['AccZ'] = convert16_32(data[data_inds[3]: data_inds[3] + 6])
    c['GyrX'] = convert16_32(data[data_inds[4]: data_inds[4] + 6])
    c['GyrY'] = convert16_32(data[data_inds[5]: data_inds[5] + 6])
    c['GyrZ'] = convert16_32(data[data_inds[6]: data_inds[6] + 6])
    # Parse Orientation data and display
    o = {}
    o['Roll'] = convert16_32(data[data_inds[7]: data_inds[7] + 6])
    o['Pitch'] = convert16_32(data[data_inds[8]: data_inds[8] + 6])
    o['Yaw'] = convert16_32(data[data_inds[9]: data_inds[9] + 6])
    # Parse Position data and display
    p = {}
    p['Latitude'] = convert16_32(data[data_inds[10]: data_inds[10] + 6])
    p['Longitude'] = convert16_32(data[data_inds[11]: data_inds[11] + 6])
    p['Altitude'] = convert16_32(data[data_inds[12]: data_inds[12] + 6])
    # Parse Velocity data and display
    v = {}
    v['VelX'] = convert16_32(data[data_inds[13]: data_inds[13] + 6])
    v['VelY'] = convert16_32(data[data_inds[14]: data_inds[14] + 6])
    v['VelZ'] = convert16_32(data[data_inds[15]: data_inds[15] + 6])
    # Parse UTC timestamp and display
    UTCTime = parse_timestamp(data[data_inds[16]: data_inds[16] + 12])
    xsens = {}
    xsens['Temp'] = t['Temp']
    xsens['Calibrated'] = c
    xsens['Orientation'] = o
    xsens['Position'] = p
    xsens['Velocity'] = v
    xsens['UTCTime'] = UTCTime
    Time_Stamp=str(UTCTime["Month"])+" "+str(UTCTime["Day"])+" "+str(UTCTime['Hour'])+":"+str(UTCTime["Minute"])+":"+str(UTCTime["Second"])+"  "+str(UTCTime["Year"])
    #localtime = time.asctime( time.localtime(time.time()))
    xsens_data=[t['Temp'],c['AccX'],c['AccY'],c['AccZ'],c['GyrX'],c['GyrY'],c['GyrZ'],o['Roll'],o['Pitch'],o['Yaw'],p['Latitude'],p    ['Longitude'],p['Altitude'],v['VelX'],v['VelY'],v['VelZ'],Time_Stamp]
    return xsens_data
    
    
    
    
    
    
